<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use App\Repository\DogRepository;
use App\Entity\SmallDog;

class HomeController extends Controller
{
    /**
     * @Route("/", name="home")
     */
    public function index(DogRepository $repo)
    {
        // $nouveauChien = new SmallDog();
        // $nouveauChien->name = "name test php";
        // $nouveauChien->breed = "race test php";
        // $nouveauChien->age = 3;
        // $repo->add($nouveauChien);
        // dump($nouveauChien);
        $result = $repo->getAll();
        return $this->render('home/index.html.twig', [
            'controller_name' => 'HomeController',
            'result'  => $result
        ]);
    }
}
