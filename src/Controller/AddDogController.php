<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use App\Entity\SmallDog;
use Symfony\Component\HttpFoundation\Request;
use App\Repository\DogRepository;
use App\Form\SmallDogType;

class AddDogController extends Controller
{
    /**
     * @Route("/add/dog", name="add_dog")
     */
    public function index(Request $request, DogRepository $repo)
    {
        //On crée le formulaire à partir de la classe Type qu'on a faite
        $form = $this->createForm(SmallDogType::class);

        //On fait la suite comme avec un formulaire normale
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            //On récupère l'instance de chien générée par le formulaire
            //avec getData() et on le donne à manger à la méthode
            //add du DogRepository qui fera persister le chien en question
            $repo->add($form->getData());
            //On fait une redirection lors d'un ajout réussi
            return $this->redirectToRoute("home");
        }


        return $this->render('add_dog/index.html.twig', [
            'form' => $form->createView()
        ]);
    }
}
