<?php

namespace App\Repository;

use App\Entity\SmallDog;
use App\Utils\ConnectUtil;


class DogRepository
{


    public function getAll() : array
    {

        //new \PDO("mysql:host=localhost:3306; dbname=mabdd", "user", "mdp");
        $dogs = [];
        try {
            /*On fait une instance de connexion à PDO en indiquant
            le lien de la base de donnée sql et son port (host). 
            Le nom de la base de donnée (dbname) qu'on veut cibler.
            Puis le nom d'utilisateur pour s'y connecter et son
            mot de passe
             */
            $cnx = ConnectUtil::getConnection();
            /*
            La méthode prepare() sur une connexion PDO va mettre une
            requête SQL en attente d'exécution à l'intérieur d'un
            objet de type PDOStatement (qu'on stock ici dans $query)
             */
            $query = $cnx->prepare("SELECT * FROM small_dog");
            //Pour la requête soit lancer, il faut lancer la méthode execute de l'objet query
            $query->execute();

            /*
            Pour récupérer les résultats de la requête, on peut 
            utiliser la méthode fetchAll qui renverra un tableau
            de tableau associatif. Chaque tableau associatif représentera une ligne de résultat.
            On fait une boucle sur le tableau de résultat pour
            faire en sorte de convertir chaque ligne de résultat
            brut en une instance de la classe voulue (ici SmallDog)
             */
            foreach ($query->fetchAll() as $row) {
                $dog = new SmallDog();
                $dog->fromSQL($row);
                $dogs[] = $dog;
            }

        } catch (\PDOException $e) {
            dump($e);
        }
        return $dogs;
    }

    public function add(SmallDog $dog)
    {
        /** connexion à PDO à externaliser dans une classe à
         * part (ou, dans une méthode au pire)
         */
        try {
            $cnx = ConnectUtil::getConnection();
            
            $query = $cnx->prepare("INSERT INTO small_dog (name, breed, age) VALUES (:name, :breed, :age)");
            
            $query->bindValue(":name", $dog->name);
            $query->bindValue(":breed", $dog->breed);
            $query->bindValue(":age", $dog->age);

            $query->execute();

            $dog->id = intval($cnx->lastInsertId());

        } catch (\PDOException $e) {
            dump($e);
        }
    }
 
    public function update(SmallDog $dog) {
        try {
            $cnx = ConnectUtil::getConnection();

            $query = $cnx->prepare("UPDATE small_dog SET name=:name, breed=:breed, age=:age WHERE id=:id");
            
            $query->bindValue(":name", $dog->name);
            $query->bindValue(":breed", $dog->breed);
            $query->bindValue(":age", $dog->age);
            $query->bindValue(":id", $dog->id);

            return $query->execute();

        } catch (\PDOException $e) {
            dump($e);
        }
        return false;
    }

    public function delete(int $id) {
        try {
            $cnx = ConnectUtil::getConnection();
            $query = $cnx->prepare("DELETE FROM small_dog WHERE id=:id");
            
            $query->bindValue(":id", $id);

            return $query->execute();

        } catch (\PDOException $e) {
            dump($e);
        }
        return false;
    }

    public function getById(int $id): ?SmallDog{
        try {
            $cnx = ConnectUtil::getConnection();

            $query = $cnx->prepare("SELECT * FROM small_dog WHERE id=:id");
            
            $query->bindValue(":id", $id);

            $query->execute();

            $result = $query->fetchAll();

            if(count($result) === 1) {
                $dog = new SmallDog();
                $dog->fromSQL($result[0]);
                return $dog;
            }

        } catch (\PDOException $e) {
            dump($e);
        }
        return null;
    }
}