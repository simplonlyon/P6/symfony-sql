<?php

namespace App\Utils;

class ConnectUtil {
    /**
     * Méthode qui se charge de faire la connexion à la bdd qui
     * pourra être utilisée par toutes les méthodes de tous
     * les repository (en considérant qu'ils se connectent tous
     * à la même bdd)
     */
    public static function getConnection(): \PDO {
        $cnx = new \PDO("mysql:host={$_ENV["MYSQL_HOST"]}:3306;dbname={$_ENV["MYSQL_DATABASE"]}", $_ENV["MYSQL_USER"], $_ENV["MYSQL_PASSWORD"]);

        $cnx->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);

        return $cnx;
    }
}
