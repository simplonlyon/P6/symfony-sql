/* On dit à SQL d'utiliser la base de données qui s'appelle db */
use db;
/*On crée une table qui s'appelle small_dog (convention de nommage : snake_case_minuscule)
On indique les différentes colonnes de la table et le type de valeur qu'elles attendent
NULL ou NOT NULL sert à dire si la colonne peut ne pas avoir de valeur
On termine en indiquant quelle est la clef primaire de notre table : ici la colonne id sera une valeur unique géré par mariadb
*/
CREATE TABLE small_dog(
    `id` INT NOT NULL AUTO_INCREMENT,
    `name` VARCHAR(40) NULL,
    `breed` VARCHAR(40) NULL,
    `age` INT NULL,
    PRIMARY KEY (id)
);

/* On ajoute une nouvelle entrée à la table small_dog
on commence par dire à quelle table on ajoute une entrée, ici small_dog
ensuite on indique entre parenthèses les colonnes auxquelles on va donner une valeur
puis on indique entre parenthèses les valeurs de ces colonnes, dans le même ordre qu'on l'a mis dans la première parenthèse
*/
INSERT INTO small_dog (name, breed, age) VALUES ("fido", "Corgi", 1);

/*Pour aller chercher des entrées d'une table, on utilise la commande SELECT, on indique
 ensuite les colonnes qu'on veut afficher (* pour toutes), puis on dit quelle table on veut requêter */
SELECT * FROM small_dog; 


/*
1) Rajouter 4 chiens dans la table small_dog, 2 qui auront plus de 5 ans, 2 qui auront comme race "patou" et un qui aura comme nom "fitzgerald"
2) Selectionner tous les chiens de la table
3) Selectionner tous les chiens dont la race est patou
4) Selectionner tous les chiens qui ont plus de 5 ans
5) Sélectionner tous les chiens dont la race est patou et qui ont plus de 3 ans
6) Sélectionner tous les chiens qui s'appelle fitzgerald ou qui sont de la race patou
*/
INSERT INTO small_dog (name, breed, age) VALUES ("fitzgerald", "dalmatian", 3);
INSERT INTO small_dog (name, breed, age) VALUES ("spot", "patou", 6);
INSERT INTO small_dog (name, breed, age) VALUES ("rex", "patou", 3);
INSERT INTO small_dog (name, breed, age) VALUES ("Roger", "le chien", 45);

SELECT * FROM small_dog;

SELECT * FROM small_dog WHERE breed="patou";

SELECT * FROM small_dog WHERE age > 5;

SELECT * FROM small_dog WHERE breed="patou" AND age > 3;

SELECT * FROM small_dog WHERE breed="patou" OR name="fitzgerald";

/**/
SELECT * FROM small_dog WHERE name LIKE "fi%";
/*
Un UPDATE permettra de mettre à jour les valeurs des colonnes d'une table.
Le plus souvent, on fera cet update en se basant sur l'id de l'élément à mettre à jour, mais on peut
utiliser n'importe quel discriminant, comme avec le SELECT (si on ne met pas de WHERE, alors toute la table sera mise à jour)
*/
UPDATE small_dog SET breed="LeChien", name="bloup" WHERE id=5;
/*
un DELETE permettra de supprimer une entrée d'une table. Comme avec l'update, on se basera sur l'id
pour supprimer, et si on omet le WHERE, la table entière sera vidée
*/
DELETE FROM small_dog WHERE id=5;

/*
Exos SQL Jointures
1. Sélectionner tous les chiens de plus de 5 ans et leur maître,
2. Sélectionner tous les chiens qui ont un maître qui s'appelle Jean
3. Sélectionner toutes les personnes qui ont un chien de race patou
4. Sélectionner toutes les personnes qui ont pas de chien ou un chien de plus de 5 ans
*/

SELECT * FROM small_dog LEFT JOIN person ON small_dog.id_person=person.id WHERE small_dog.age > 5;


SELECT name,COUNT(name) FROM person GROUP BY name;

SELECT * FROM small_dog LEFT JOIN person ON small_dog.id_person=person.id WHERE person.surname= "Jean";

SELECT * FROM person LEFT JOIN small_dog ON small_dog.id_person=person.id WHERE small_dog.breed='patou';
